(function(){

    var selectCIty = document.querySelector('.js-selectCity');
    
    // listen selectbox changes
    selectCIty.addEventListener("change", function(e){
        var val = e.target.value;
        var detailsWrap = document.querySelector('.js-detailsWrap');

        // hide details wrapper
        detailsWrap.style.display = 'none';

        // if user select 'Current Location' -> get user location
        // and then get weather
        if(val == 'current'){
            getUserLocation();
        }
        // in case when user selected another city  ->
        //  get Wather data for that city
        else if(locationsInfo[val]){
            var city = locationsInfo[val];
            getWeather(city.lat, city.lon);
        }
    }, false);

    // bind events to all info elements dynamicly 
    document.addEventListener('click', function (e) {  
        var infoClass = 'info';
        var target = e.target;

        // if event target have info class
        if (target.classList.contains(infoClass)) {
            // get index from attribute
            var index = target.getAttribute('data-index');
            
            // get current object using index
            if(currentDataObject[index]){
                var that = currentDataObject[index];
                var details = document.querySelector('.js-details');
                var detailsWrap = document.querySelector('.js-detailsWrap');
                // some hardcoded data
                var detailsToShow = {
                    sunset: getTime(that.sunsetTime),
                    sunrise: getTime(that.sunriseTime),
                    tempMin: that.temperatureMin + DEG_HTML,
                    tempMax: that.temperatureMax + DEG_HTML,
                    windSpeed: that.windSpeed
                };
                
                // clean up details div 
                details.innerHTML = '';

                // create element for each details
                for(var key in detailsToShow){
                    var p = document.createElement('p');
                    p.innerHTML = key + ': ' + detailsToShow[key];
                    details.appendChild(p);
                }

                // show details wrapper
                detailsWrap.style.display = 'block';

                // scroll to details block
                window.location = '#details';
            }
        }
    }, false);

})();
