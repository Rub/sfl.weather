/*
    Make XHR request

    Void: do not returns anything
    runs callback instead
 */
function request(url, method, cb){
    if(url){
        // if method is not provided
        if (typeof method === "function") cb = method;
        // set default value for method
        method = (method && typeof method === 'string') ? method : 'GET';

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);

        if(method.toUpperCase == 'POST') xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
                cb(xhr.responseText);
            }
        }

        xhr.send();
    }
}

/*
    Get time from Unix timestamp and format it
    Return: String
*/
function getTime(date){
    date = new Date(date * 1000);

    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = date.getMinutes();
    minutes = (minutes < 10) ? '0' + minutes : minutes;

    return hours + ':' + minutes;
}

/*
    Get date from Unix timestamp and format it
    Return: String
*/
function getDate(date){
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    date = new Date(date * 1000);
    
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    date = day + ' ' + month + ' ' + year;

    return date;
}


/*
    Count Average temperature 
    Return: Number 
 */
function countAverageTemperature(max, min){
    var sum = (max + min) / 2;
    return Math.round(sum);
}

/*
    Count Average temperature for upcoming days
    Return: Number 
 */
function countGeneralAverageTemperature(data){
    if(data){
        var sum = 0;

        for(var i = 0; i < data.length; i++){
            var that = data[i];
            var temp = countAverageTemperature(that.temperatureMax, that.temperatureMin);
            sum += temp;
        }

        sum = sum / data.length
        return Math.round(sum);
    }
    return NaN;
}
