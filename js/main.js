// GLOBAL VARIABLES AND CONSTANTS
var currentDataObject;
var IP_API_URL = 'http://ip-api.com/json';
var DARK_SKY_ACCESS_KEY = '629b5c5e37934aa55533fdb37201f0de';
var DARK_SKY_BASE_URL = 'https://api.darksky.net/forecast/';
var DEG_HTML =' &deg; F'; 
var locationsInfo = {
    "london": {
        lat: 51.5287718,
        lon: -0.2416804
    },
    "paris": {
        lat: 48.8589507,
        lon: 2.2775175
    },
    "berlin": {
        lat: 52.5076682,
        lon: 13.2860631
    },
    "madrid": {
        lat: 40.4381311,
        lon: -3.8196197
    },
    "rome": {
        lat: 41.9102415,
        lon: 12.395915
    }
};

/*
    Attach Event to info button
    Return: Element
 */
function attachEvent(elem){
    if(elem){
        elem.addEventListener("click", function (e){
            // get index from attribute
            var index = e.target.getAttribute('data-index');
            
            // get current object using index
            if(currentDataObject[index]){
                var that = currentDataObject[index];
                var details = $getByID('details');
                var detailsWrap = $getByID('detailsWrap');
                // some hardcoded data
                var detailsToShow = {
                    sunset: getTime(that.sunsetTime),
                    sunrise: getTime(that.sunriseTime),
                    tempMin: that.temperatureMin + DEG_HTML,
                    tempMax: that.temperatureMax + DEG_HTML,
                    windSpeed: that.windSpeed
                };
                
                // clean up details div 
                details.innerHTML = '';

                // create element for each details
                for(var key in detailsToShow){
                    var p = document.createElement('p');
                    p.innerHTML = key + ': ' + detailsToShow[key];
                    details.appendChild(p);
                }

                // show details wrapper
                detailsWrap.style.display = 'block';

                // scroll to details block
                window.location = '#details';
            }
        }, false);

        return elem;
    }    
}

/*
    Pick icon for weather using iconText
    Return: String
*/
function pickWeatherIcon(iconText){
    var className = '';
    var weathers = {
        'day-sunny': 'wi-forecast-io-clear-day',
        'night-clear': 'wi-forecast-io-clear-night',
        'day-cloudy': 'wi-forecast-io-partly-cloudy-day',
        'night-cloudy': 'wi-forecast-io-partly-cloudy-night', 
        'rain': 'wi-forecast-io-rain', 
        'snow': 'wi-forecast-io-snow', 
        'sleet': 'wi-forecast-io-sleet', 
        'strong-wind': 'wi-forecast-io-wind',
        'fog': 'wi-forecast-io-fog', 
        'cloudy': 'wi-forecast-io-cloudy', 
        'hail': 'wi-forecast-io-hail', 
        'thunderstorm': 'wi-forecast-io-thunderstorm', 
        'tornado': 'wi-forecast-io-tornado' 
    };
    
    for (key in weathers){
        if(key.indexOf(iconText) != -1){
            className = weathers[key];
        }
    }
    
    // set default className in case if nothing matches
    className = className || 'wi-forecast-io-clear-day';

    return '<i class="wi '+ className + '"></i> ';
}

/*
    Create weather block using data
    Return: Element
 */
function createWeatherBlock(index, data){
    if(data){
        var date, div, dateElement,
            summaryText, info, average,
            averageTemp;

        date = getDate(data.time);
        averageTemp = countAverageTemperature(data.temperatureMax, data.temperatureMin);
        
        // create main weather block
        div = document.createElement('div');        
        div.className = 'weatherBlock';
        
        // create date element
        dateElement = document.createElement('p')
        dateElement.innerHTML = date;
        div.appendChild(dateElement);

        // create element for average temperature 
        average = document.createElement('h2');
        average.innerHTML = pickWeatherIcon(data.icon) + averageTemp + DEG_HTML;
        div.appendChild(average);
        
        //create element for summary text 
        summaryText = document.createElement('h4');
        summaryText.innerHTML = data.summary;
        div.appendChild(summaryText);

        // create info element
        info = document.createElement('span');
        info.className = 'info';
        info.setAttribute('data-index', index);
        info.innerText = 'i';
        info = attachEvent(info);
        div.appendChild(info);

        return div;
    }
}
/*
    Display the received data
    VOID: no callbacks 
 */
function showWeather(daily){
    if(daily && daily.data){
        var sum,
            generalSum,
            that = daily.data,
            weatherWrap = $getByID('weatherWrap');

        // update Global object
        window.currentDataObject = daily.data;

        // cleaning weatherWrap
        weatherWrap.innerHTML = '';

        for(var i = 0; i < that.length; i++){
            // create weather block for each day
            var div = createWeatherBlock(i, that[i]);
            
            weatherWrap.appendChild(div);
        }

        // show average temperature for upcoming days: 
        generalSum = document.createElement('h3');
        sum = countGeneralAverageTemperature(that);
        generalSum.innerHTML = 'Average Temperature for upcoming days: ' + sum + DEG_HTML;

        weatherWrap.appendChild(generalSum);
    }
}

/*
    Get weather forecast data from Dark Sky
    VOID: with callback
 */
function getWeather(lat, lon) {
    var url = DARK_SKY_BASE_URL + DARK_SKY_ACCESS_KEY + '/' + lat + ',' + lon;
    console.log(url);
    
    request(url, function(data){
        data = JSON.parse(data);
        console.log(data);

        showWeather(data.daily);
    });
}

/*
    Get user location info using ip-api.com API
    VOID: with CB
 */
function getLocationByIP() {
    request(IP_API_URL, function(data){
        data = JSON.parse(data);
        console.log(data);

        getWeather(data.lat, data.lon);
    });
}

function getUserLocation() {
    // geolocation is available
    if ("geolocation" in navigator) {
        function getPosition(position) {
            getWeather(position.coords.latitude, position.coords.longitude);
        }

        function failed(){
            console.log('Unable to retrieve your location using HTML 5 Geo API');
            getLocationByIP();
        }

        navigator.geolocation.getCurrentPosition(getPosition, failed);
    } 
    // geolocation IS NOT available. Get location using IP
    else {
        getLocationByIP();
    }
}

// initial function. App starts here
function init() {
    console.log('window loaded!');
    // check if browser supports XHR.
    // in the next step we will do AJAX request, so XHR support is must
    if(checkXHRSupport()){
        getUserLocation();
    }
}

window.onload = init;
