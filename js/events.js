(function(){

    var selectCIty = $getByID('selectCity');
    
    // listen selectbox changes
    selectCIty.addEventListener("change", function(e){
        var val = e.target.value;
        var detailsWrap = $getByID('detailsWrap');

        // hide details wrapper
        detailsWrap.style.display = 'none';

        // if user select 'Current Location' -> get user location
        // and then get weather
        if(val == 'current'){
            getUserLocation();
        }
        // in case when user selected another city  ->
        //  get Wather data for that city
        else if(locationsInfo[val]){
            var city = locationsInfo[val];
            getWeather(city.lat, city.lon);
        }
    }, false);

})();
