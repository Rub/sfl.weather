# SFL - Weather Forecast

Simple weather forecast website written in pure *html, css and js.*

To run it locally you need any static server (like https://www.npmjs.com/package/static-server).

**Also make sure you have enabled CORS**.
You can use extention like (https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/?src=userprofile).
Unfortunately Dark Sky do not have *JSONP*.
